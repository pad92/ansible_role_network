# network

Set /etc/hostname, /etc/hosts and /etc/resolv.conf

put /etc/hosts custom entries into /etc/hosts.d/99_custom

## Requirements

none

## Role Variables

```
cts_role_network:
  dns_server: ['194.6.240.1', '194.6.240.1']
  dns_domain: localdomain.local
  dns_search: localdomain.local
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - network
```


